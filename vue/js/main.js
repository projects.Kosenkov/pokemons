new Vue({
  el: '#filter',
  data: {
    all: 'Все покемоны',
    grass: 'Стихия природы',
    fire: 'Стихия огня',
    water: 'Стихия воды',
  }
});


document.addEventListener("DOMContentLoaded", function () {
  showPokemons();
  filterPokemons();
});

function showPokemons() {
  let allPokemonContainer = document.querySelector('#poke-container')
  allPokemonContainer.innerText = "";
  createPokemons();
};


function filterPokemons() {
  let filter_select_el = document.getElementById('filter');
  let items_el = document.getElementById('poke-container');

  filter_select_el.onchange = function () {
    console.log(this.value);
    let items = items_el.getElementsByClassName('card');
    for (let i = 0; i < items.length; i++) {

      if (items[i].classList.contains(this.value)) {
        items[i].style.display = 'block';
      } else {
        items[i].style.display = 'none';
      }

    }
  }
}

function createPokemons() {
  fetch('https://pokeapi.co/api/v2/pokemon?limit=20&offset=0')
    .then(response => response.json())
    .then(function (allpokemon) {
      allpokemon.results.forEach(function (pokemon) {
        fetchPokemonData(pokemon);
      });
    });
};

function fetchPokemonData(pokemon) {
  let url = pokemon.url;
  fetch(url)
    .then(response => response.json())
    .then(function (pokeData) {
      renderPokemon(pokeData);
    })
};


function renderPokemon(pokeData) {
  let allPokemonContainer = document.getElementById('poke-container');
  let pokeContainer = document.createElement("div")
  pokeContainer.classList.add('ui', 'card');

  createPokeImage(pokeData.id, pokeContainer);

  let pokeName = document.createElement('h2');
  pokeName.innerText = pokeData.name;

  let pokeNumber = document.createElement('p');
  pokeNumber.innerText = `#${pokeData.id}`;

  let pokeTypes = document.createElement('ul');

  createTypes(pokeData.types, pokeTypes, pokeContainer);

  pokeContainer.append(pokeName, pokeNumber, pokeTypes);
  allPokemonContainer.appendChild(pokeContainer);
};

function createTypes(types, ul, pokeContainer) {
  types.forEach(function (type) {
    let typeLi = document.createElement('li');
    typeLi.innerText = type['type']['name'];
    if (typeLi.innerText == 'grass') pokeContainer.classList.add('grass');
    if (typeLi.innerText == 'fire') pokeContainer.classList.add('fire');
    if (typeLi.innerText == 'water') pokeContainer.classList.add('water');
    ul.append(typeLi);
  })
};

function createPokeImage(pokeID, containerDiv) {
  let pokeImgContainer = document.createElement('div')
  pokeImgContainer.classList.add('image')

  let pokeImage = document.createElement('img');
  pokeImage.srcset = `https://pokeres.bastionbot.org/images/pokemon/${pokeID}.png`;

  pokeImgContainer.append(pokeImage);
  containerDiv.append(pokeImgContainer);
};





